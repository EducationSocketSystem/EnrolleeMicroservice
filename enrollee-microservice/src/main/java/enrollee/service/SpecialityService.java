/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import vertexprize.es.core.document.Speciality;
import vertexprize.es.core.objectcontainer.ObjectContainer;

/**
 *
 * @author user
 */
@ApplicationScoped
public class SpecialityService {
        
    @Inject
    EntityManager em;

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<List<Speciality>> getAllSpeciality() {

        ObjectContainer<List<Speciality>> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода getAllSpeciality");

        List<Speciality> specialities = new LinkedList<>();

        container.addInfos("Получение списка всех специальностей " + Speciality.class.getSimpleName() + " ...");//Speciality.class.getSimpleName() для перевода в string
        try {
            specialities = (List<Speciality>) em.createQuery("Select t from " + Speciality.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка специальностей: " + e.getMessage());
        }
        container.addInfos("Проверка листа специальностей  на пустоту ...");
        if (specialities.isEmpty()) {
            container.addWarning("Лист специальностей  пуст");
        } else {
            container.addInfos("Получен список специальностей из [" + specialities.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех специальностей  прошло успешно");
            container.setValue(specialities);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех специальностей не был получен");
        }

        return container;
    }
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Speciality> getSpeciality(UUID id) {

        ObjectContainer<Speciality> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getSpeciality");

        Speciality speciality = null;

        if (id != null) {
            container.addInfos("Получение специальности с UUID= " + id + " ...");
            try {
                speciality = em.find(Speciality.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении специальности: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения специальности: входной экземпляр UUID пустой");
        }

        if (speciality != null && container.getErrors().isEmpty()) {
            container.addInfos("Получена специальности " + speciality.toString());
            container.addInfos("Получение специальности прошло успешно");
            container.setValue(speciality);
            container.setPresent(true);
        } else {
            container.addInfos("специальности не была найден");
        }

        return container;

    }
    
@Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Void> saveSpeciality(Speciality speciality) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции saveSpeciality");
        String nameOperation = "Сохранение/обновление";

        if (speciality != null) {
            if (speciality.getUid() == null) {
                container.addInfos("Сохранение новой специальности " + speciality.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(speciality);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении специальности: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения специальности ...");
                ObjectContainer<Speciality> containerGet = this.getSpeciality(speciality.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения специальности: специальность не была записана в базе данных");
                }
            } else {
                container.addInfos("Поиск специальности в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<Speciality> containerGet = this.getSpeciality(speciality.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление специальности в базе данных c " + containerGet.getValue().toString() + " на " + speciality.toString() + " ...");
                    try {
                        em.merge(speciality);
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении специальности: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления специальности ...");
                    containerGet = this.getSpeciality(speciality.getUid());
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение специальности, которая была в базе данных " + containerGet.getValue().toString() + " с обновляемой " + speciality.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(speciality)) {
                        container.addError("Ошибка обновления специальности: специальность не изменилось c " + containerGet.getValue().toString() + " на " + speciality.toString());
                    }
                } else {
                    container.addError("Ошибка обновления специальности: специальность с UUID=" + speciality.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения специальности: входной экземпляр " + Speciality.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + "Сохранение специальности в базе данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + "Сохранение специальности в базе данных не прошло успешно");
        }

        return container;
    }
    @Transactional
    public ObjectContainer<Void> deleteSpeciality(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода deleteSpeciality");

        if (id != null) {
            container.addInfos("Поиск специальности в бд ...");
            ObjectContainer<Speciality> containerGet = this.getSpeciality(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                Speciality speciality = containerGet.getValue();
                container.addInfos("Удаление специальности " + speciality.toString() + " из базы данных ...");
                try {
                    em.remove(em.merge(speciality));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении специальности: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления специальности ...");
                containerGet = this.getSpeciality(speciality.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
            }
        } else {
            container.addError("Ошибка удаления специальности: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление специальности из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление специальности из базы данных не прошло успешно");
        }

        return container;
    }
}
