/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import vertexprize.es.core.document.SchoolCertificate;
import vertexprize.es.core.objectcontainer.ObjectContainer;

/**
 *
 * @author user
 */
@ApplicationScoped
public class SchoolCertificateService {
        
    @Inject
    EntityManager em;

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<List<SchoolCertificate>> getAllSchoolCertificate() {

        ObjectContainer<List<SchoolCertificate>> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода getAllSchoolCertificate");

        List<SchoolCertificate> schoolCertificates = new LinkedList<>();

        container.addInfos("Получение списка всех школьных сертификатов " + SchoolCertificate.class.getSimpleName() + " ...");//SchoolCertificate.class.getSimpleName() для перевода в string
        try {
            schoolCertificates = (List<SchoolCertificate>) em.createQuery("Select t from " + SchoolCertificate.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка школьных сертификатов: " + e.getMessage());
        }
        container.addInfos("Проверка листа школьных сертификатов  на пустоту ...");
        if (schoolCertificates.isEmpty()) {
            container.addWarning("Лист школьных сертификатов  пуст");
        } else {
            container.addInfos("Получен список школьных сертификатов из [" + schoolCertificates.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех школьных сертификатов  прошло успешно");
            container.setValue(schoolCertificates);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех школьных сертификатов не был получен");
        }

        return container;
    }
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<SchoolCertificate> getSchoolCertificate(UUID id) {

        ObjectContainer<SchoolCertificate> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getSchoolCertificate");

        SchoolCertificate schoolCertificate = null;

        if (id != null) {
            container.addInfos("Получение школьных сертификатов с UUID= " + id + " ...");
            try {
                schoolCertificate = em.find(SchoolCertificate.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении школьных сертификатов: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения школьных сертификатов: входной экземпляр UUID пустой");
        }

        if (schoolCertificate != null && container.getErrors().isEmpty()) {
            container.addInfos("Получен школьный сертификат " + schoolCertificate.toString());
            container.addInfos("Получение школьного сертификата прошло успешно");
            container.setValue(schoolCertificate);
            container.setPresent(true);
        } else {
            container.addInfos("школьный сертификат не был найден");
        }

        return container;

    }
    
@Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Void> saveSchoolCertificate(SchoolCertificate schoolCertificate) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции saveSchoolCertificate");
        String nameOperation = "Сохранение/обновление";

        if (schoolCertificate != null) {
            if (schoolCertificate.getUid() == null) {
                container.addInfos("Сохранение нового школьного сертификата " + schoolCertificate.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(schoolCertificate);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении школьного сертификата: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения школьного сертификата ...");
                ObjectContainer<SchoolCertificate> containerGet = this.getSchoolCertificate(schoolCertificate.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения школьного сертификата: школьный сертификат не был записан в базе данных");
                }
            } else {
                container.addInfos("Поиск школьного сертификата в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<SchoolCertificate> containerGet = this.getSchoolCertificate(schoolCertificate.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление школьного сертификата в базе данных c " + containerGet.getValue().toString() + " на " + schoolCertificate.toString() + " ...");
                    try {
                        em.merge(schoolCertificate);
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении школьного сертификата: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления школьного сертификата...");
                    containerGet = this.getSchoolCertificate(schoolCertificate.getUid());
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение школьного сертификата, который был в базе данных " + containerGet.getValue().toString() + " с обновляемым " + schoolCertificate.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(schoolCertificate)) {
                        container.addError("Ошибка обновления школьного сертификата: школьный сертификат не изменился c " + containerGet.getValue().toString() + " на " + schoolCertificate.toString());
                    }
                } else {
                    container.addError("Ошибка обновления школьного сертификата: кольный сертификат с UUID=" + schoolCertificate.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения кольного сертификата: входной экземпляр " + SchoolCertificate.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + "Сохранение школьного сертификата в базе данных прошёл успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + "Сохранение школьного сертификата в базе данных не прошёл успешно");
        }

        return container;
    }
    @Transactional
    public ObjectContainer<Void> deleteSchoolCertificate(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода deleteSchoolCertificate");

        if (id != null) {
            container.addInfos("Поиск школьного сертификата в бд ...");
            ObjectContainer<SchoolCertificate> containerGet = this.getSchoolCertificate(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                SchoolCertificate schoolCertificate = containerGet.getValue();
                container.addInfos("Удаление школьного сертификата " + schoolCertificate.toString() + " из базы данных ...");
                try {
                    em.remove(em.merge(schoolCertificate));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении школьного сертификата: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления школьного сертификата ...");
                containerGet = this.getSchoolCertificate(schoolCertificate.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
            }
        } else {
            container.addError("Ошибка удаления школьного сертификата: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление школьного сертификата из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление школьного сертификата из базы данных не прошло успешно");
        }

        return container;
    }
}
