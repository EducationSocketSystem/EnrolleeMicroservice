/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.jboss.logging.Logger;
import vertexprize.es.core.enrollee.Enrollee;
import vertexprize.es.core.objectcontainer.ObjectContainer;

/**
 *
 * @author Mokshin K.S.
 */
@ApplicationScoped//указывает на то, что управляемый бин будет доступен на протяжении времени жизни приложения

public class EnrolleeService {

    private static final Logger log = Logger.getLogger(EnrolleeService.class);

    @Inject
    EntityManager em;//экземпляр класса EntityManager для загрузки и сохранения сущностей
    //Сохраняет экземпляр класса в таблицу

    /**
     * Метод получения списка отчётов<br>
     *
     * Для получения списка абитуриентов (и записи в enrollees) используется
     * запрос createQuery класса EntityManager Если было получено исключение, то
     * в поле errors(шпаргалка в ObjectContainer) мы записываем сообщение об
     * этом исключении<br>
     *
     * Дальше идёт условие проверки на пустоту. Если список окажется пустым, то
     * выведется сообщение об этом, и так же это работает, если список
     * заполнен<br>
     *
     * Последним условием является проверка списка ошибок на пустоту Если ошибки
     * отсутствуют, то метод выдаст нам сообщение об успешности получения целого
     * списка абитуриентов, поле setValue запишет информацию о целом списке
     * абитуриентов, а поле SetPresent выдаст true В противном случае, экземпляр
     * класса ObjectContainer выдаст нам информацию, что процесс завершился
     * неудачно<br>
     *
     * ObjectContainer<List<Enrollee>>(Enrollee - это хранит экземпляр класса)
     * container = new ObjectContainer<>();
     *
     * @return результат в контейнере
     */
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<List<Enrollee>> getAllEnrollee() {

        ObjectContainer<List<Enrollee>> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода getAllEnrollee");

        List<Enrollee> enrollees = new LinkedList<>();

        container.addInfos("Получение списка всех абитуриентов " + Enrollee.class.getSimpleName() + " ...");//Enrollee.class.getSimpleName() для перевода в string
        try {
            enrollees = (List<Enrollee>) em.createQuery("Select t from " + Enrollee.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка абитуриентов: " + e.getMessage());
        }
        container.addInfos("Проверка листа абитуриентов  на пустоту ...");
        if (enrollees.isEmpty()) {
            container.addWarning("Лист абитуриентов  пуст");
        } else {
            container.addInfos("Получен список абитуриентов из [" + enrollees.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех абитуриентов  прошло успешно");
            container.setValue(enrollees);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех абитуриентов не был получен");
        }

        return container;
    }

    /**
     * Транзакция по получению абитуриента из базы данных <br>
     *
     * С самого начала транзакции входной параметр id проверяется на равенство
     * null Если это поле не null, то для получения абитуриента применяется
     * запрос em.find класса EntityManager Если он равен null, то в
     * ObjectContainer записывается ошибка Если было получено исключение, то оно
     * записывается в поле ошибкок экземпляра класса ObjectContainer.<br>
     *
     * В самом конце экземпляр класса Enrollee проверяется на null и поле ошибок
     * экземпляра класса ObjectContainer проверяется на пустоту.<br>
     *
     * Если условие выполняется, то в поле информационных сообщений фиксируется
     * информация о том, что абитуриент был получен и операция прошла успешно В
     * противном случае, если один из полей равно false, то в поле
     * информационных сообщений фиксируется информация о том, что абитуриент не
     * был найден. <br>
     * Поле value записывается экземпляр класса Enrollee, а поле present
     * записшет true.
     *
     * @param id уникальный номер абитуриента
     * @return результат в контейнере
     */
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Enrollee> getEnrollee(UUID id) {

        ObjectContainer<Enrollee> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getEnrollee");

        Enrollee enrollee = null;

        if (id != null) {
            container.addInfos("Получение абитуриента с UUID= " + id + " ...");
            try {
                enrollee = em.find(Enrollee.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении абитуриента: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения абитуриента: входной экземпляр UUID пустой");
        }

        if (enrollee != null && container.getErrors().isEmpty()) {
            container.addInfos("Получен абитуриент " + enrollee.toString());
            container.addInfos("Получение абитуриента прошло успешно");
            container.setValue(enrollee);
            container.setPresent(true);
        } else {
            container.addInfos("абитуриент не был найден");
        }

        return container;

    }

    /**
     * Определяет область действия одной транзакции БД Транзакция БД происходит
     * внутри области действий persistence context Persistence context в JPA
     * является EntityManager
     *
     * Транзакция по сохранению экземпляра класса AbstractReport в базе
     * данных.<br>
     * Перед операцией по сохранению абитуриента входной параметр report
     * проверяется на равенство null.<br>
     *
     * Если он равен null, то в ObjectContainer записывается ошибка.<br>
     *
     * Если поле id экземпляра класса AbstractReport равен null, то в базе
     * данных создается новая запись абитуриента, иначе происходит слияние с
     * существующей.<br>
     *
     * Для создания новой записи в базе данных используется метод persist класса
     * EntityManager.<br>
     *
     * После операции по созданию записи проверяется корректность создания
     * записи.<br>
     *
     * Для проверки корректности создания используется тракзакция по получению
     * абитуриента по id. Информационные сообщения, предупреждения и ошибки,
     * полученные при выполнении транзакции, записываются в экземпляр класса
     * ObjectContainer.<br>
     *
     * Если поле present экземпляр класса ObjectContainer равно false, то в
     * ObjectContainer записывается ошибка.<br>
     *
     * Для слияния экземпляра класса AbstractReport с записью в базе данных
     * используется метод merge класса EntityManager.<br>
     *
     * Перед операции по слиянию в базе данных ищется запись с id, равной id
     * экземпляра класса AbstractReport.<br>
     *
     * Если такая запись найдена, то входной экземпляр сливается с записью в
     * базе данных.<br>
     *
     * Если при поиске записи в базе данных с id, равной id экземпляра класса
     * AbstractReport, запись не была найдена, то в ObjectContainer записывается
     * ошибка.<br>
     *
     * После операции по слиянию проверяется корректность слияния.<br>
     *
     * Если запись в базе данных неравна экземпляру класса AbstractReport, то в
     * ObjectContainer записывается ошибка.<br>
     *
     * Если при выполнении создания или слияния записи было поймано исключение,
     * то оно записывается в поле ошибкок экземпляра класса ObjectContainer.<br>
     *
     * Перед завершением транзакции поле ошибок экземпляра класса
     * ObjectContainer проверяется на пустоту, и если оно содержет ошибки, то в
     * поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об неудачной попытке сохранения абитуриента.<br>
     *
     * Иначе в поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об удачной попытке сохранения абитуриента. В поле
     * present записывается true.
     *
     *
     * @param enrollee сохраняет запись в объект класса
     * @return результат в контейнере
     */
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Void> saveEnrollee(Enrollee enrollee) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции saveEnrollee");
        String nameOperation = "Сохранение/обновление";

        if (enrollee != null) {
            if (enrollee.getUid() == null) {
                container.addInfos("Сохранение нового абитуриента " + enrollee.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(enrollee);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении абитуриента: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения абитуриента ...");
                ObjectContainer<Enrollee> containerGet = this.getEnrollee(enrollee.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения абитуриента: абитуриент не был записан в базе данных");
                }
            } else {
                container.addInfos("Поиск абитуриента в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<Enrollee> containerGet = this.getEnrollee(enrollee.getUid());//абитуриент изначальный(без изменений)
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление абитуриента в базе данных c " + containerGet.getValue().toString() + " на " + enrollee.toString() + " ...");
                    try {
                        em.merge(enrollee);//обновление записи абитуриента в БД(в enrollee данные на обновление)
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении абитуриента: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления абитуриента ...");
                    containerGet = this.getEnrollee(enrollee.getUid());//абитуриент обновлённый
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение абитуриента, который был в базе данных " + containerGet.getValue().toString() + " с обновляемым " + enrollee.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(enrollee))//сравниваем новые данные абитуриента с теми, которые хранятся в БД
                    {
                        container.addError("Ошибка обновления абитуриента: абитуриент не изменился c " + containerGet.getValue().toString() + " на " + enrollee.toString());
                    }
                } else {
                    container.addError("Ошибка обновления абитуриента: абитуриент с UUID=" + enrollee.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения абитуриента: входной экземпляр " + Enrollee.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + "Сохранение абитуриента в базе данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + "Сохранение абитуриента в базе данных не прошло успешно");
        }

        return container;

    }

    /**
     * Транзакция по удалению записи абитуриента из базы данных.<br><br>
     *
     * Перед операцией по удалению абитуриента входной параметр id проверяется
     * на равенство null.<br>
     *
     * Если он равен null, то в ObjectContainer записывается ошибка.<br>
     *
     * Если неравен null, то при помощи транзакции по получению абитуриента
     * проверяется наличие в базе данных записи с id, равной id входного
     * параметра.<br>
     *
     * Если запись не находится, то в ObjectContainer записывается ошибка.<br>
     *
     * Иначе при помощи метода remove класса EntityManager запись удаляется из
     * базы данных.<br>
     *
     * После операции по удалению абитуриента проверяется корректность
     * удаления.<br>
     *
     *
     * При помощи транзакции по получению абитуриента проверяется наличие в базе
     * данных записи с id, равной id входного параметра.<br>
     *
     * Если такая запись находится, то в ObjectContainer записывается
     * ошибка.<br>
     *
     * Перед завершением транзакции поле ошибок экземпляра класса
     * ObjectContainer проверяется на пустоту, и если оно содержет ошибки, то в
     * поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об неудачной попытке удаления абитуриента. <br>
     *
     * Иначе в поле информационных сообщений экземпляра класса ObjectContainer
     * записывается информация об удачной попытке удаления абитуриента. В поле
     * present записывается true.
     *
     * @param id экземпляр класса UUID
     * @return ObjectContainer &lt;Void&gt;
     */
    @Transactional
    public ObjectContainer<Void> deleteEnrollee(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода deleteEnrollee");

        if (id != null) {
            container.addInfos("Поиск абитуриента в бд ...");
            ObjectContainer<Enrollee> containerGet = this.getEnrollee(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                Enrollee enrollee = containerGet.getValue();
                container.addInfos("Удаление абитуриента " + enrollee.toString() + " из базы данных ...");
                try {
                    em.remove(em.merge(enrollee));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении абитуриента: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления абитуриента ...");
                containerGet = this.getEnrollee(enrollee.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
            }
        } else {
            container.addError("Ошибка удаления абитуриента: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление абитуриента из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление абитуриента из базы данных не прошло успешно");
        }

        return container;
    }

    /**
     * Транзакция по удалению записи абитуриента из базы данных.<br><br>
     *
     * Перед операцией по сохранению абитуриента входной параметр report
     * проверяется на равенство null.<br>
     *
     * Если он равен null, то в ObjectContainer записывается ошибка.<br>
     *
     * Если неравен null, то при помощи транзакции по получению абитуриента
     * проверяется наличие в базе данных записи с id, равной id входного
     * экземпляра класса AbstractReport.<br>
     *
     * Если запись не находится, то в ObjectContainer записывается ошибка.<br>
     *
     * Иначе при помощи транзакции по удалению абитуриента по id запись
     * удаляется из базы данных.<br>
     *
     * Перед завершением транзакции поле ошибок экземпляра класса
     * ObjectContainer проверяется на пустоту, и если оно не содержет ошибки, то
     * в поле present записывается true.
     *
     * @param enrollee экземпляр класса AbstractReport
     * @return ObjectContainer &lt;Void&gt;
     */
    @Transactional
    public ObjectContainer<Void> deleteEnrollee(Enrollee enrollee) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции deleteEnrollee по удалению абитуриента");

        if (enrollee != null) {
            ObjectContainer<Void> containerDelete = this.deleteEnrollee(enrollee.getUid());
            container.addAllInfos(containerDelete.getInfos());
            if (!containerDelete.getWarnings().isEmpty()) {
                container.addAllWarnings(containerDelete.getWarnings());
            }
            if (!containerDelete.getErrors().isEmpty()) {
                container.addAllErrors(containerDelete.getErrors());
            }
        } else {
            container.addError("Ошибка удаления абитуриента: входной экземпляр Enrollee пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.setPresent(true);
        }

        return container;
    }

}
