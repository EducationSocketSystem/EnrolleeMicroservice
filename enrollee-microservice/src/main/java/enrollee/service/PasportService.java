/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import vertexprize.es.core.document.Pasport;
import vertexprize.es.core.objectcontainer.ObjectContainer;

/**
 *
 * @author user
 */
@ApplicationScoped
public class PasportService {
        
    @Inject
    EntityManager em;

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<List<Pasport>> getAllPassport() {

        ObjectContainer<List<Pasport>> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода getAllPassport");

        List<Pasport> passports = new LinkedList<>();

        container.addInfos("Получение списка всех паспортов " + Pasport.class.getSimpleName() + " ...");//Passport.class.getSimpleName() для перевода в string
        try {
            passports = (List<Pasport>) em.createQuery("Select t from " + Pasport.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка паспортов: " + e.getMessage());
        }
        container.addInfos("Проверка листа паспортов  на пустоту ...");
        if (passports.isEmpty()) {
            container.addWarning("Лист паспортов  пуст");
        } else {
            container.addInfos("Получен список паспортов из [" + passports.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех паспортов  прошло успешно");
            container.setValue(passports);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех паспортов не был получен");
        }

        return container;
    }
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Pasport> getPassport(UUID id) {

        ObjectContainer<Pasport> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getPassport");

        Pasport passport = null;

        if (id != null) {
            container.addInfos("Получение паспорта с UUID= " + id + " ...");
            try {
                passport = em.find(Pasport.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении паспорта: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения паспорта: входной экземпляр UUID пустой");
        }

        if (passport != null && container.getErrors().isEmpty()) {
            container.addInfos("Получен паспорт " + passport.toString());
            container.addInfos("Получение паспорта прошло успешно");
            container.setValue(passport);
            container.setPresent(true);
        } else {
            container.addInfos("паспорт не был найден");
        }

        return container;

    }
    
@Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Void> savePassport(Pasport passport) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции savePassport");
        String nameOperation = "Сохранение/обновление";

        if (passport != null) {
            if (passport.getUid() == null) {
                container.addInfos("Сохранение нового паспорта " + passport.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(passport);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении паспорта: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения паспорта ...");
                ObjectContainer<Pasport> containerGet = this.getPassport(passport.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения паспорта: паспорт не был записан в базе данных");
                }
            } else {
                container.addInfos("Поиск паспорта в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<Pasport> containerGet = this.getPassport(passport.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление паспорта в базе данных c " + containerGet.getValue().toString() + " на " + passport.toString() + " ...");
                    try {
                        em.merge(passport);
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении паспорта: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления паспорта ...");
                    containerGet = this.getPassport(passport.getUid());
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение паспорта, который был в базе данных " + containerGet.getValue().toString() + " с обновляемым " + passport.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(passport)) {
                        container.addError("Ошибка обновления паспорта: паспорт не изменился c " + containerGet.getValue().toString() + " на " + passport.toString());
                    }
                } else {
                    container.addError("Ошибка обновления паспорта: паспорт с UUID=" + passport.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения паспорта: входной экземпляр " + Pasport.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + "Сохранение паспорта в базе данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + "Сохранение паспорта в базе данных не прошло успешно");
        }

        return container;
    }
    @Transactional
    public ObjectContainer<Void> deletePassport(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода deletePassport");

        if (id != null) {
            container.addInfos("Поиск паспорта в бд ...");
            ObjectContainer<Pasport> containerGet = this.getPassport(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                Pasport passport = containerGet.getValue();
                container.addInfos("Удаление паспорта " + passport.toString() + " из базы данных ...");
                try {
                    em.remove(em.merge(passport));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении паспорта: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления паспорта ...");
                containerGet = this.getPassport(passport.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
            }
        } else {
            container.addError("Ошибка удаления паспорта: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление паспорта из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление паспорта из базы данных не прошло успешно");
        }

        return container;
    }
}
