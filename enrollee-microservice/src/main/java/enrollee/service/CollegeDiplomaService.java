/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import vertexprize.es.core.document.CollegeDiploma;
import vertexprize.es.core.objectcontainer.ObjectContainer;

/**
 *
 * @author user
 */
@ApplicationScoped
public class CollegeDiplomaService {

    @Inject
    EntityManager em;//использует jdbc driver для доступа в posttgres

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<List<CollegeDiploma>> getAllCollegeDiploma() {

        ObjectContainer<List<CollegeDiploma>> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода getAllCollegeDiploma");

        List<CollegeDiploma> collegeDiplomas = new LinkedList<>();

        container.addInfos("Получение списка всех дипломов " + CollegeDiploma.class.getSimpleName() + " ...");//CollegeDiploma.class.getSimpleName() для перевода в string
        try {
            collegeDiplomas = (List<CollegeDiploma>) em.createQuery("Select t from " + CollegeDiploma.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка дипломов: " + e.getMessage());
        }
        container.addInfos("Проверка листа дипломов  на пустоту ...");
        if (collegeDiplomas.isEmpty()) {
            container.addWarning("Лист дипломов  пуст");
        } else {
            container.addInfos("Получен список дипломов из [" + collegeDiplomas.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех дипломов  прошло успешно");
            container.setValue(collegeDiplomas);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех дипломов не был получен");
        }

        return container;
    }
    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<CollegeDiploma> getCollegeDiploma(UUID id) {

        ObjectContainer<CollegeDiploma> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getCollegeDiploma");

        CollegeDiploma diploma = null;

        if (id != null) {
            container.addInfos("Получение диплома с UUID= " + id + " ...");
            try {
                diploma = em.find(CollegeDiploma.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении диплома: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения диплома: входной экземпляр UUID пустой");
        }

        if (diploma != null && container.getErrors().isEmpty()) {
            container.addInfos("Получен диплом " + diploma.toString());
            container.addInfos("Получение диплома прошло успешно");
            container.setValue(diploma);
            container.setPresent(true);
        } else {
            container.addInfos("диплом не был найден");
        }

        return container;

    }

    @Transactional
    public ObjectContainer<Void> saveCollegeDiploma(CollegeDiploma collegeDiploma) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции saveCollegeDiploma");
        String nameOperation = "Сохранение/обновление";

        if (collegeDiploma != null) {
            if (collegeDiploma.getUid() == null) {
                container.addInfos("Сохранение нового диплома " + collegeDiploma.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(collegeDiploma);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении диплома: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения диплома ...");
                ObjectContainer<CollegeDiploma> containerGet = this.getCollegeDiploma(collegeDiploma.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения диплома: диплом не был записан в базе данных");
                }
            } else {
                container.addInfos("Поиск диплома в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<CollegeDiploma> containerGet = this.getCollegeDiploma(collegeDiploma.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление диплома в базе данных c " + containerGet.getValue().toString() + " на " + collegeDiploma.toString() + " ...");
                    try {
                        em.merge(collegeDiploma);
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении диплома: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления диплома ...");
                    containerGet = this.getCollegeDiploma(collegeDiploma.getUid());
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение диплома, который был в базе данных " + containerGet.getValue().toString() + " с обновляемым " + collegeDiploma.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(collegeDiploma)) {
                        container.addError("Ошибка обновления диплома: диплом не изменился c " + containerGet.getValue().toString() + " на " + collegeDiploma.toString());
                    }
                } else {
                    container.addError("Ошибка обновления диплома: диплом с UUID=" + collegeDiploma.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения диплома: входной экземпляр " + CollegeDiploma.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + "Сохранение диплома в базе данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + "Сохранение диплома в базе данных не прошло успешно");
        }

        return container;
    }
        @Transactional
    public ObjectContainer<Void> deleteCollegeDiploma(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода deleteCollegeDiploma");

        if (id != null) {
            container.addInfos("Поиск диплома в бд ...");
            ObjectContainer<CollegeDiploma> containerGet = this.getCollegeDiploma(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                CollegeDiploma collegeDiploma = containerGet.getValue();
                container.addInfos("Удаление диплома " + collegeDiploma.toString() + " из базы данных ...");
                try {
                    em.remove(em.merge(collegeDiploma));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении диплома: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления диплома ...");
                containerGet = this.getCollegeDiploma(collegeDiploma.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
            }
        } else {
            container.addError("Ошибка удаления диплома: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление диплома из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление диплома из базы данных не прошло успешно");
        }

        return container;
    }
}
