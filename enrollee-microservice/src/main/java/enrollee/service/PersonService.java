/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.jboss.logging.Logger;
import vertexprize.es.core.objectcontainer.ObjectContainer;
import vertexprize.es.core.person.Person;

/**
 *
 * @author Mokshin K.S.
 */
@ApplicationScoped//указывает на то, что управляемый бин будет доступен на протяжении времени жизни приложения

public class PersonService {

    private static final Logger log = Logger.getLogger(PersonService.class);

    @Inject
    EntityManager em;//экземпляр класса EntityManager для загрузки и сохранения сущностей
    //Сохраняет экземпляр класса в таблицу

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<List<Person>> getAllPerson() {

        ObjectContainer<List<Person>> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода getAllPerson");

        List<Person> persons = new LinkedList<>();

        container.addInfos("Получение списка всех персон " + Person.class.getSimpleName() + " ...");//Person.class.getSimpleName() для перевода в string
        try {
            persons = (List<Person>) em.createQuery("Select t from " + Person.class.getSimpleName() + " t").getResultList();
        } catch (Exception e) {
            container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении списка персон: " + e.getMessage());
        }
        container.addInfos("Проверка листа персон  на пустоту ...");
        if (persons.isEmpty()) {
            container.addWarning("Лист персон  пуст");
        } else {
            container.addInfos("Получен список персон из [" + persons.size() + "] записи/записей");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Получение списка всех персон  прошло успешно");
            container.setValue(persons);
            container.setPresent(true);
        } else {
            container.addInfos("Список всех персон не был получен");
        }

        return container;
    }

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Person> getPerson(UUID id) {

        ObjectContainer<Person> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции getPerson");

        Person  person = null;

        if (id != null) {
            container.addInfos("Получение персоны с UUID= " + id + " ...");
            try {
                person = em.find(Person.class, id);
            } catch (Exception e) {
                container.addError("Ошибка " + e.getClass().getSimpleName() + " при получении персоны: " + e.getMessage());
            }
        } else {
            container.addError("Ошибка получения персоны: входной экземпляр UUID пустой");
        }

        if (person != null && container.getErrors().isEmpty()) {
            container.addInfos("Получен персона " + person.toString());
            container.addInfos("Получение персоны прошло успешно");
            container.setValue(person);
            container.setPresent(true);
        } else {
            container.addInfos("персона не был найден");
        }

        return container;

    }

    @Transactional//определяет область действия одной транзакции БД
    public ObjectContainer<Void> savePerson(Person person) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции savePerson");
        String nameOperation = "Сохранение/обновление";

        if (person != null) {
            if (person.getUid() == null) {
                container.addInfos("Сохранение новой персоны " + person.toString() + " в базе данных ...");
                nameOperation = "Сохранение";
                try {
                    em.persist(person);
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при сохранении персоны: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности сохранения персоны ...");
                ObjectContainer<Person> containerGet = this.getPerson(person.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (!containerGet.isPresent()) {
                    container.addError("Ошибка сохранения персоны: персона не был записан в базе данных");
                }
            } else {
                container.addInfos("Поиск персоны в бд ...");
                nameOperation = "Обновление";
                ObjectContainer<Person> containerGet = this.getPerson(person.getUid());//персона изначальная(без изменений)
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
                if (containerGet.isPresent()) {

                    container.addInfos("Обновление персоны в базе данных c " + containerGet.getValue().toString() + " на " + person.toString() + " ...");
                    try {
                        em.merge(person);//обновление записи персоны в БД(в person данные на обновление)
                    } catch (Exception e) {
                        container.addError("Ошибка " + e.getClass().getSimpleName() + " при обновлении персоны: " + e.getMessage());
                    }

                    container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                    em.flush();
                    container.addInfos("Очиста контекста персистенции ...");
                    em.clear();

                    container.addInfos("Проверка корректности обновления персоны ...");
                    containerGet = this.getPerson(person.getUid());//персона обновлённый
                    container.addAllInfos(containerGet.getInfos());
                    if (!containerGet.getWarnings().isEmpty()) {
                        container.addAllWarnings(containerGet.getWarnings());
                    }
                    if (!containerGet.getErrors().isEmpty()) {
                        container.addAllErrors(containerGet.getErrors());
                    }
                    container.addInfos("Сравнение персоны, которая был в базе данных " + containerGet.getValue().toString() + " с обновляемым " + person.toString() + " ...");
                    if (containerGet.isPresent() && containerGet.getValue().equals(person))//сравниваем новые данные персоны с теми, которые хранятся в БД
                    {
                        container.addError("Ошибка обновления персоны: персона не изменилась c " + containerGet.getValue().toString() + " на " + person.toString());
                    }
                } else {
                    container.addError("Ошибка обновления персоны: персона с UUID=" + person.getUid() + " не существует в базе данных");
                }
            }
        } else {
            container.addError("Ошибка сохранения персоны: входной экземпляр " + Person.class.getSimpleName() + " пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos(nameOperation + "Сохранение персоны в базе данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos(nameOperation + "Сохранение персоны в базе данных не прошло успешно");
        }

        return container;

    }

    @Transactional
    public ObjectContainer<Void> deletePerson(UUID id) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы метода deletePerson");
//        em.remove(id);
        if (id != null) {
            container.addInfos("Поиск персоны в бд ...");
            ObjectContainer<Person> containerGet = this.getPerson(id);
            container.addAllInfos(containerGet.getInfos());
            if (!containerGet.getWarnings().isEmpty()) {
                container.addAllWarnings(containerGet.getWarnings());
            }
            if (!containerGet.getErrors().isEmpty()) {
                container.addAllErrors(containerGet.getErrors());
            }
            if (containerGet.isPresent()) {
                Person person = containerGet.getValue();
                container.addInfos("Удаление персоны " + person.toString() + " из базы данных ...");
                try {
                    em.remove(em.merge(person));
                } catch (Exception e) {
                    container.addError("Ошибка " + e.getClass().getSimpleName() + " при удалении персоны: " + e.getMessage());
                }
                container.addInfos("Синхронизация контекста персистенции с базой данных ...");
                em.flush();
                container.addInfos("Очиста контекста персистенции ...");
                em.clear();

                container.addInfos("Проверка корректности удаления персоны ...");
                containerGet = this.getPerson(person.getUid());
                container.addAllInfos(containerGet.getInfos());
                if (!containerGet.getWarnings().isEmpty()) {
                    container.addAllWarnings(containerGet.getWarnings());
                }
                if (!containerGet.getErrors().isEmpty()) {
                    container.addAllErrors(containerGet.getErrors());
                }
            }
        } else {
            container.addError("Ошибка удаления персоны: входной экземпляр UUID пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.addInfos("Удаление персоны из базы данных прошло успешно");
            container.setPresent(true);
        } else {
            container.addInfos("Удаление персоны из базы данных не прошло успешно");
        }

        return container;
    }

    @Transactional
    public ObjectContainer<Void> deletePerson(Person person) {

        ObjectContainer<Void> container = new ObjectContainer<>();
        container.addInfos("Начало работы транзакции deletePerson по удалению персоны");

        if (person != null) {
            ObjectContainer<Void> containerDelete = this.deletePerson(person.getUid());
            container.addAllInfos(containerDelete.getInfos());
            if (!containerDelete.getWarnings().isEmpty()) {
                container.addAllWarnings(containerDelete.getWarnings());
            }
            if (!containerDelete.getErrors().isEmpty()) {
                container.addAllErrors(containerDelete.getErrors());
            }
        } else {
            container.addError("Ошибка удаления персоны: входной экземпляр Person пустой");
        }

        if (container.getErrors().isEmpty()) {
            container.setPresent(true);
        }

        return container;
    }

}
