/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.controller;

import enrollee.service.CollegeDiplomaService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import vertexprize.es.core.document.CollegeDiploma;
import vertexprize.es.core.enrollee.Enrollee;
import vertexprize.es.core.objectcontainer.ObjectContainer;

/**
 *
 * @author user
 */
@Path("/diplom")
public class CollegeDiplomaController {

    private static final Logger log = Logger.getLogger(CollegeDiplomaController.class.getName());
    @Inject
    CollegeDiplomaService collegeDiplomaService;

    @Consumes(MediaType.APPLICATION_JSON)//Требует, чтобы полученный объект был в JSON-формате
    //Для внесения данных в таблицу(либо удаление)
    @POST//принимает запрос POST
    public void createCollegeDiploma(CollegeDiploma collegeDiploma) {

        log.info("Поступил запрос на создание в базе данных нового отчета " + collegeDiploma.toString());

        ObjectContainer<Void> container = collegeDiplomaService.saveCollegeDiploma(collegeDiploma);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }

    }

    //Для получения запросов в таблице
    @GET//принимает запрос GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<CollegeDiploma> getAllCollegeDiplomas() {

        log.info("Поступил запрос на получение полного списка отчетов " + Enrollee.class.getSimpleName());
        List<CollegeDiploma> collegeDiplomas = new ArrayList<>();

        ObjectContainer<List<CollegeDiploma>> container = collegeDiplomaService.getAllCollegeDiploma();
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }

        return container.getValue();
    }

    @Path("/{id}")
    @DELETE
    public void deleteCollegeDiploma(@PathParam("id") UUID id) {

        log.info("Поступил запрос на удаление из базы данных отчета c UUID= " + id);

        ObjectContainer<Void> container = collegeDiplomaService.deleteCollegeDiploma(id);
        log.info(container.printInfos("Содержимое списка информационных сообщений"));
        if (!container.getWarnings().isEmpty()) {
            log.warn(container.printWarnings("Содержимое списка предупреждений"));
        }
        if (!container.getErrors().isEmpty()) {
            log.error(container.printErrors("Содержимое списка ошибок"));
        }
    }

}
