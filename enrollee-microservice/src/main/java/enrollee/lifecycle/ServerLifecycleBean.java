/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package enrollee.lifecycle;

import enrollee.service.CollegeDiplomaService;
import enrollee.service.EnrolleeService;
import enrollee.service.PasportService;
import enrollee.service.PersonService;
import enrollee.service.SchoolCertificateService;
import enrollee.service.SpecialityService;
import io.quarkus.runtime.StartupEvent;
import java.time.LocalDate;
import org.jboss.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import vertexprize.es.core.document.CollegeDiploma;
import vertexprize.es.core.enrollee.Enrollee;
import vertexprize.es.core.person.Person;

/**
 *
 * @author student
 */
@ApplicationScoped
public class ServerLifecycleBean {

    private static final Logger log = Logger.getLogger(ServerLifecycleBean.class.getName());

    @Inject
    CollegeDiplomaService collegeDiplomaService;
    @Inject
    EnrolleeService enrolleeService;
    @Inject
    PasportService passportService;
    @Inject
    SchoolCertificateService schoolCertificateService;
    @Inject
    SpecialityService specializationService;
    @Inject
    PersonService personService;

    //для подписки на события
    //Запускается микросервис, далее идёт подписка на событие, которое вызовет метод
    // Запускается ли сервер? Для этого нужен данный метод
    void onStart(@Observes StartupEvent event) {

        // Формирование даты дня рождения
        LocalDate birthDate1 = LocalDate.of(2000, 06, 26);

        Enrollee enrollee = new Enrollee();//создание пустого экземпляра с помощью new(оператор)
        enrollee.setPassword("12345678");
        enrollee.setName("Константин");
        enrollee.setSureName("Мокшин");
        enrollee.setMiddleName("Сергеевич");
        enrollee.setBirthDate(birthDate1);
        enrollee.setLoginString("Mokshin.K.S");
        enrollee.setEmail("kostya_mokshin@mail.ru");
        log.info("Завершено создание экземляра класса Enrollee и заполнение его полей");

        //Сохранение записи в базе данных 
        enrolleeService.saveEnrollee(enrollee);

        log.info("Создан абитуриент и его id = " + enrollee.getUid());

        // Формирование даты получения отчёта        
        LocalDate issueDate = LocalDate.of(2001, 07, 27);

        log.info("Попытка создания документа");
        //нет UID, пока не сохранится в БД при помощи service
        CollegeDiploma diploma = new CollegeDiploma();
        diploma.setDocNumber("777");
        diploma.setIssueDate(issueDate);
        diploma.setIssueOrgName("TOGU");
        diploma.setDocName("диплом колледжа");
        diploma.setCityString("khavarovsk");
        diploma.setQualification("programmer");
        diploma.setSpeciality("IT");
        diploma.setSupervisor("Марина Ивановна Иванова");
        diploma.setСhairman("Иванова Ирина Ивановна");

        log.info("Завершено создание экземляра класса CollegeDiploma и заполнение его полей");
        //Сохранение записи в базе данных
        collegeDiplomaService.saveCollegeDiploma(diploma);

        log.info("Создан документ и его id = " + diploma.getUid());

        log.info("Попытка создания документа");
        //нет UID, пока не сохранится в БД при помощи service
        CollegeDiploma diploma2 = new CollegeDiploma();
        diploma2.setDocNumber("777");
        diploma2.setIssueDate(issueDate);
        diploma2.setIssueOrgName("TOGU");
        diploma2.setDocName("диплом колледжа");
        diploma2.setCityString("khavarovsk");
        diploma2.setQualification("programmer");
        diploma2.setSpeciality("IT");
        diploma2.setSupervisor("Марина Ивановна Иванова");
        diploma2.setСhairman("Иванова Ирина Ивановна");

        log.info("Завершено создание экземляра класса CollegeDiploma и заполнение его полей");
        //Сохранение записи в базе данных
        collegeDiplomaService.saveCollegeDiploma(diploma2);
        log.info("Создан документ и его id = " + diploma.getUid());

        // Формирование даты дня рождения
        LocalDate birthDate = LocalDate.of(2000, 06, 26);

        log.info("[" + event.toString() + "] Сервер EnrolleeMicroservice. Старт сервера ... ");
        Person person = new Person();
        person.setPassword("12345678");
        person.setName("Константин");
        person.setSureName("Мокшин");
        person.setMiddleName("Сергеевич");
        person.setBirthDate(birthDate);
        person.setLoginString("Mokshin.K.S");
        person.setEmail("kostya_mokshin@mail.ru");
        log.info("Завершено создание экземляра класса Enrollee и заполнение его полей");

        log.info("Попытка создания персоны");
        personService.savePerson(person);

        person.getCollegeDiplomaList().add(diploma);//получили список и в него добавили
        person.getCollegeDiplomaList().add(diploma2);
        personService.savePerson(person);

//        Не работает удаление диплома из таблицы(10 июня)
//        log.info("Список дипломов до удаления ["+ person.getCollegeDiplomaList().size() +"] записей");
//        person.getCollegeDiplomaList().remove(diploma2);//украл из списка
//        log.info("Список дипломов после удаления ["+ person.getCollegeDiplomaList().size() +"] записей");
//        personService.savePerson(person);
    }

}

/*
  log.info("Получение записи документа с id = " + diploma.getUid() + " ...");
        ObjectContainer<CollegeDiploma> container = new ObjectContainer();
        container = collegeDiplomaService.getCollegeDiploma(diploma.getUid());//получение данных по заданному id из БД. Затем данные кладутся в контейнер
        if (!container.isPresent())//присутсвует ли запись
        {
            log.info("Получение записи с id = " + diploma.getUid() + " неудачно");
        } else {

            CollegeDiploma diplomfFromDB = container.getValue();//присваиваем значение CollegeDiploma
            log.info("Успешно найдена запись в БД с id = " + diplomfFromDB.getUid());
            log.info("Привязываем абитуриента к документу");
            //diplomfFromDB.setEnrollee(enrollee);//записываем данные абитуриента в документ
            log.info("Сохраняем документ в БД");
            collegeDiplomaService.saveCollegeDiploma(diplomfFromDB);
            
        }
 */
//        collegeDiplomaService.saveCollegeDiploma(diploma2);
//        collegeDiplomaService.saveCollegeDiploma(diploma1);
//        CollegeDiploma diploma2 = new CollegeDiploma();
//        CollegeDiploma diploma1 = new CollegeDiploma();
//
//        LocalDate date = LocalDate.of(2000, 06, 26);
//        diploma2.setDocNumber("AB1312");
//        diploma2.setIssueDate(date);
//
//        diploma1.setDocNumber("AB1313");
//        date = LocalDate.of(2001, 07, 27);
//        diploma1.setIssueDate(date);
//        CollegeDiploma diploma2 = new CollegeDiploma();
//        CollegeDiploma diploma1 = new CollegeDiploma();
//
//        LocalDate date = LocalDate.of(2000, 06, 26);
//        diploma2.setDocNumber("AB1312");
//        diploma2.setIssueDate(date);
//
//        diploma1.setDocNumber("AB1313");
//        date = LocalDate.of(2001, 07, 27);
//        diploma1.setIssueDate(date);
//        CollegeDiploma diploma2 = new CollegeDiploma();
//        CollegeDiploma diploma1 = new CollegeDiploma();
//
//        LocalDate date = LocalDate.of(2000, 06, 26);
//        diploma2.setDocNumber("AB1312");
//        diploma2.setIssueDate(date);
//
//        diploma1.setDocNumber("AB1313");
//        date = LocalDate.of(2001, 07, 27);
//        diploma1.setIssueDate(date);
//        Passport passport = new Passport();
//        passportService.savePassport(passport);
//
//        Photo photo = new Photo();
//        photoService.savePhoto(photo);
//
//        SchoolCertificate schoolCertificate = new SchoolCertificate();
//        schoolCertificateService.saveSchoolCertificate(schoolCertificate);
//
//        Speciality speciality = new Speciality();
//        specializationService.saveSpeciality(speciality);
//
//        diploma1.setEnrollee(enrollee);
//        diploma2.setEnrollee(enrollee);
